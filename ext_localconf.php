<?php
if(!defined("TYPO3_MODE"))
	die("Access denied.");

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['additionalBackendItems']['cacheActions'][] = 'EXT:realurl_clearcache/Classes/ClearCacheHook.php:&Mobasoft\\RealurlClearcache\\ClearCacheHook';
$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['clearCachePostProc'][] = 'EXT:realurl_clearcache/Classes/ClearCacheHook.php:&Mobasoft\\RealurlClearcache\\ClearCacheHook->clear';
