<?php
namespace Mobasoft\RealurlClearcache;

/***************************************************************
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 *
 * @version $Id$
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 */

class ClearCacheHook implements \TYPO3\CMS\Backend\Toolbar\ClearCacheActionsHookInterface {

	/**
	 * Add an entry to the CacheMenuItems array
	 *
	 * @param array $cacheActions Array of CacheMenuItems
	 * @param array $optionValues Array of AccessConfigurations-identifiers (typically used by userTS with options.clearCache.identifier)
	 * @return void
	 */
	public function manipulateCacheActions(&$cacheActions, &$optionValues){
		if($GLOBALS['BE_USER']->isAdmin()){
			$cacheActions[] = array(
				'id'    => 'realurl_clearcache',
				'title' => "Clear RealURL Cache",
				'href'  => $this->backPath . 'tce_db.php?vC=' . $GLOBALS['BE_USER']->veriCode() . '&cacheCmd=realurl_clearcache&ajaxCall=1' . \TYPO3\CMS\Backend\Utility\BackendUtility::getUrlToken('tceAction'),
				'icon'  => \TYPO3\CMS\Backend\Utility\IconUtility::getSpriteIcon('actions-system-cache-clear-impact-low')
			);
		}
	}

	/**
	 * This method is called by the CacheMenuItem in the Backend
	 * @param \array $_params
	 * @param \TYPO3\CMS\Core\DataHandling\DataHandler $dataHandler
	 */
	public static function clear($_params, $dataHandler){
		if(in_array($_params['cacheCmd'], array("all", "realurl_clearcache")) && $GLOBALS['BE_USER']->isAdmin()){
			$clearTables = array(
				'tx_realurl_pathcache',
				'tx_realurl_uniqalias',
				'tx_realurl_urlcache'
			);

			foreach($clearTables as $table){
				$GLOBALS['TYPO3_DB']->sql_query('TRUNCATE TABLE ' . $table . ';');
			}
		}
	}

}
